<?php

namespace Drupal\archivesspace\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\media\Entity\Media;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Imports ArchivesSpace Digital Object file_versions.
 *
 * Creates a media entity and returns the entity id.
 *
 * @MigrateProcessPlugin(
 *   id = "archivesspace_import_media"
 * )
 */
class ImportMedia extends ProcessPluginBase implements ContainerFactoryPluginInterface {


  /**
   * File repository for copying files.
   *
   * @var fileRepo \Drupal\file\FileRepositoryInterface
   * */
  protected $fileRepo;

  /**
   * Create plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface.
   * @param array $configuration
   *   Configration.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   *
   * @return static
   *   Plugin instance.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file.repository')
    );
  }

  /**
   * Plugin constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   For copying files to Drupal.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileRepositoryInterface $fileRepo) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileRepo = $fileRepo;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      throw new MigrateSkipProcessException();
    }
    if (!is_array($value)) {
      throw new MigrateException(sprintf('ArchivesSpace ImportMedia process failed for destination property (%s): input is not an array.', $destination_property));
    }

    if ($this->configuration['drupal_path'] === TRUE) {
      $files = \Drupal::entityTypeManager()
        ->getStorage('file')
        ->loadByProperties(['uri' => $value['file_uri']]);
      $file = reset($files);
    }
    else {
      if (!$file = $this->fileRepo->writeData($value['file_uri'], pathinfo($value['file_uri'], PATHINFO_BASENAME))) {
        throw new MigrateException(sprintf('ArchivesSpace Import Media could not download the file version uri "%s".', $value['file_uri']));
      }
    }

    if (intval($file->getSize()) < 1) {
      $file->delete();
      throw new MigrateException(sprintf('ArchivesSpace Import Media received zero bytes from URI "%s"', $value['file_uri']));
    }
    // We assume image types.
    // @todo Support multiple Use Statements.
    $media = Media::create([
      'bundle' => 'image',
      'uid' => \Drupal::currentUser()->id(),
      'langcode' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
      'field_media_image' => [
        'target_id' => $file->id(),
        'alt' => $value['caption'],
        'title' => $value['caption'],
      ],
    ]);
    $media->save();
    return $media->id();

  }

}
